package exnihilo.compatibility.foresty;

import forestry.api.apiculture.FlowerManager;

public enum FlowerType {
	None("flowersNone"),
	Normal(FlowerManager.FlowerTypeVanilla),
	Nether(FlowerManager.FlowerTypeNether),
	End(FlowerManager.FlowerTypeEnd),
	Jungle(FlowerManager.FlowerTypeJungle),
	Mushroom(FlowerManager.FlowerTypeMushrooms),
	Gourd(FlowerManager.FlowerTypeGourd),
	Cactus(FlowerManager.FlowerTypeCacti),
	Wheat(FlowerManager.FlowerTypeWheat),
	Water("flowersWater");

	private final String forestryKey;

	FlowerType(String forestryKey) {
		this.forestryKey = forestryKey;
	}

	public String getForestryKey() {
		return forestryKey;
	}
}
