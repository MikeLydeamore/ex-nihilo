package exnihilo.world;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFalling;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.ChunkProviderHell;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.terraingen.PopulateChunkEvent;
import exnihilo.data.WorldData;

public class ChunkProviderHellVoid extends ChunkProviderHell {
	
	private World world;
	private Random rand;
	
	public ChunkProviderHellVoid(World par1World, long par2) {
		super(par1World, par2);
		this.world = par1World;
		this.rand = new Random(world.getSeed());
	}

    @Override
    public void func_147419_a(int par1, int par2, Block[] par3ArrayOfBlock)
    {
    	//Do nothing.
    }

    @Override
    public void func_147418_b(int par1, int par2, Block[] par3ArrayOfBlock)
    {
    	//Do nothing.
    }
    
    @Override
    public void populate(IChunkProvider provider, int x, int z)
    {
    	if (WorldData.allowNetherFortresses)
    	{
    		super.populate(provider, x, z);
    	}
    	else
    	{
    		BlockFalling.fallInstantly = true;

    		this.rand.setSeed(this.world.getSeed());
    		long i1 = this.rand.nextLong() / 2L * 2L + 1L;
    		long j1 = this.rand.nextLong() / 2L * 2L + 1L;
    		this.rand.setSeed((long)x * i1 + (long)z * j1 ^ world.getSeed());

    		MinecraftForge.EVENT_BUS.post(new PopulateChunkEvent.Pre(provider, world, rand, x, z, false));

    		MinecraftForge.EVENT_BUS.post(new PopulateChunkEvent.Post(provider, world, rand, x, z, false));
    		BlockFalling.fallInstantly = false;
    	}
    }
}
