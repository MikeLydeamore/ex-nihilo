package exnihilo.registries.helpers;

import exnihilo.utils.ItemInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;

@Data
@AllArgsConstructor
public class FluidItemCombo {
	
	private Fluid inputFluid;
	private ItemInfo inputItem;
	
	public FluidItemCombo(Fluid inputFluid, ItemStack inputStack)
	{
		this.inputFluid = inputFluid;
		this.inputItem = new ItemInfo(inputStack);
	}

}
