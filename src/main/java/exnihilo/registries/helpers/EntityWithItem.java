package exnihilo.registries.helpers;

import net.minecraft.item.ItemStack;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor
public class EntityWithItem {
	
	@SuppressWarnings("rawtypes")
	private Class entity;
	private ItemStack drops;
	private String particle;

}
