package exnihilo.utils;

import java.util.LinkedHashSet;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.config.Configuration;
import exnihilo.ENBlocks;
import exnihilo.ENItems;
import exnihilo.data.ModData;
import exnihilo.registries.helpers.Color;

public class CrookUtils {

	private static LinkedHashSet<ItemInfo> blacklist = new LinkedHashSet<ItemInfo>(); 
	
	public static void load(Configuration config) {
		for (String input : ModData.CROOK_BLACKLIST)
		{
			String[] current = input.split(":");
			if (current.length == 3 && Block.blockRegistry.getObject(current[0]+":"+current[1]) != null)
			{
				Block block = (Block) Block.blockRegistry.getObject(current[0]+":"+current[1]);
				blacklist.add(new ItemInfo(block, Integer.parseInt(current[2])));
			}
		}
	}
	
	/**
	 * Performs the "crooking" on a block. Note: Does not actually break the block.
	 * The return value can be used to determine if extra durability should be taken.
	 * @param item The itemstack used to break the block
	 * @param X x-coordinate of block
	 * @param Y y-coordinate of block
	 * @param Z z-coordinate of block
	 * @param player
	 * @return true if crooking was successful and extra drops called, false if block can not be crooked.
	 */
	public static boolean doCrooking(ItemStack item, int X, int Y, int Z, EntityPlayer player)
	{
		World world = player.worldObj;
		Block block = world.getBlock(X,Y,Z);
		int meta = world.getBlockMetadata(X, Y, Z);
		boolean validTarget = false;
		if (block.isLeaves(world, X, Y, Z) && !blacklist.contains(new ItemInfo(block, meta)))
		{
			if (!world.isRemote)
			{
				//Call it once here and it gets called again when it breaks.
				block.dropBlockAsItem(world, X, Y, Z, meta, 0);

				//Silkworms
				if (ModData.ALLOW_SILKWORMS && world.rand.nextInt(100) == 0)
				{
					world.spawnEntityInWorld(new EntityItem(world, X + 0.5D, Y + 0.5D, Z + 0.5D, new ItemStack(ENItems.Silkworm, 1, 0)));
				}
			}

			validTarget = true;
		}

		if (block == ENBlocks.LeavesInfested)
		{
			if (!world.isRemote)
			{
				if (ModData.ALLOW_SILKWORMS && world.rand.nextInt(15) == 0)
				{
					world.spawnEntityInWorld(new EntityItem(world, X + 0.5D, Y + 0.5D, Z + 0.5D, new ItemStack(ENItems.Silkworm, 1, 0)));
				}
			}

			validTarget = true;
		}
		

		return validTarget;

	}
}
